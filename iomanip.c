#include "iomanip.h"
#include "stdlib.h"
#include "stdio.h"

char * Tab(int n) {
    char * str = (char*)malloc((n+1) * sizeof(char));
    str[n] = '\0';
    for (int i = 0; i < n; i++)
        str[i] = '\t';
    
    return str;
}

char * ToString(const Data const * d) {
    char * str = (char*)malloc((BUFFER_SIZE + 1) * sizeof(char));
    
    switch(d->m_type) {
        case _INT:{
            int * pI = (int*)d->m_pData;
            sprintf(str, "%d", *pI);
            break;
        }
        case _DOUBLE:{
            double * dI = (double *)d->m_pData;
            sprintf(str, "%9.2f", *dI);
            break;
        }
        case _STRING:{
            char * str_data = (char*)(d->m_pData);
            memcpy(str, str_data, strlen(str_data));
            break;
        }       
    }
    
    // Mettre le caractere de fin de chaine
    str[strlen(str)] = '\0';
    
    return str;
}

void PrintCell(const Data * d) {
    char * str = ToString(d);
    printf(FORMAT_CELL, str);
}

void PrintHeaders(const Table const * t)
{
    const TableHeader * hdrs = GetHeaders(t);
    if (hdrs == NULL)
        return;
    
    // Print the HEaders
    for (int i = 0; i < t->m_colCount; i++)
    {
        printf(FORMAT_CELL, hdrs[i].m_caption);
    }
    
    printf("\n");
    
    for (int i = 0; i < t->m_colCount; i++)
        printf(SEPARATOR);
    
    printf("\n");
}

void PrintRow(const Table * t, int row)
{
    // Monte une liste des Data de la row en ordre de colonne.
    const Data * curRow = GetRow(t, row);
    
    if (curRow == NULL)
        return;
    
    // Print the row
    for (int i = 0; i < t->m_colCount; i++)
        PrintCell( & curRow[i] );
    
    printf("\n");
}
void PrintTable(const Table const * t) 
{
    PrintHeaders(t);
    for (int i = 1; i <= t->m_rowCount; i++)
        PrintRow(t, i);
    
    printf("\n");
}