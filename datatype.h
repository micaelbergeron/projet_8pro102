/* 
 * File:   datatype.h
 * Author: Micael
 *
 * Created on 3 décembre 2011, 13:57
 */

#ifndef DATATYPE_H
#define	DATATYPE_H

#define DYNARRAY(type, size) (type *) malloc (size * sizeof(type))
#define TRUE  1
#define FALSE 0

// Les types de row possibles.
typedef enum dataType{
    _INT = 0,
    _DOUBLE = 1,
    _STRING = 2
} DataType;

static const char * FORMAT_CELL = "| %-10s |";
static const char * SEPARATOR   = "==============";

typedef struct data {
    void *      m_pData;
    DataType    m_type;
} Data;

typedef struct tableHeader
{
    char* m_key;
    char* m_caption;
} TableHeader;

typedef struct tableColumn
{
    int                 m_active;
    TableHeader         m_header;
    Data *              m_rows; // Data as Void *
    int                 m_rowCount;
    enum dataType       m_type;
} TableColumn;

typedef struct table
{
    TableColumn*           m_columns;
    int*                   m_active; // Flag for deletion
    int                    m_colCount;
    int                    m_rowCount;
    int                    m_rowArraySize;
    int                    m_colArraySize;
} Table;


#endif	/* DATATYPE_H */

