#include "Table.h"

#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <io.h>
// Functions


// La table débute avec une colonne booléenne qui indique si la ligne est
// encore active.
Table * CreateTable(int count, ...)
{
    Table * t = (Table *)malloc(sizeof(Table));
    t->m_columns = DYNARRAY(TableColumn, count);
    t->m_active = NULL;
    t->m_colCount = count;
    t->m_colArraySize = count;
    t->m_rowCount = 0;
    t->m_rowArraySize = 0;
    
    va_list va;
    va_start(va, count);
    int i;
    for (i = 0; i < count; i++)
        t->m_columns[i] = va_arg(va, TableColumn);

    va_end(va);

    return t;
}

int AddColumns(Table * t, int count, ...)
{
    int newCount = t->m_colCount + count;
    // Fait le transfer des colonnes existantes
    TableColumn * newCol = DYNARRAY(TableColumn, newCount);
    memcpy(newCol, t->m_columns, t->m_colCount * sizeof(TableColumn));

    va_list va;
    va_start(va, count);
    
    int i;
    for(i = t->m_colCount; i < newCount; i++)
    {
        // Il faut s'assurer que la nouvelle colonne aura le même nombre de row que les autres
        TableColumn tc = va_arg(va, TableColumn);
        
        Data * tableRows = DYNARRAY(Data, t->m_rowArraySize);
        memcpy(tableRows, tc.m_rows, tc.m_rowCount * sizeof(Data));
        
        for (int i = tc.m_rowCount; i < t->m_rowArraySize; i++) {
            tableRows[i].m_pData = NULL;
            tableRows[i].m_type = tc.m_type;
        }
        
        tc.m_rows = tableRows;
        tc.m_rowCount = t->m_rowCount;
        
        newCol[i] = tc;
    }

    va_end(va);

    // Cleanup

    t->m_colCount = t->m_colArraySize = newCount;
    t->m_columns = newCol;
    
    return newCount;
}

int AddRows(Table * t, int count)
{
    int newCount = t->m_rowCount + count;
    
    // Il faut updater les flags des lignes.
    int * flags = DYNARRAY(int, newCount);
    memcpy(flags, t->m_active, t->m_rowCount * sizeof(TableColumn));

    for (int i = t->m_rowCount; i < newCount; i++)
        flags[i] = TRUE;
    
    t->m_active = flags;

    for (int i = 0; i < t->m_colCount; i++) {
        int rowCount = t->m_rowCount;

        // On copie d'abord la mémoire existante dans une array plus grande.
        Data * rows = DYNARRAY(Data, newCount);
        memcpy(rows, t->m_columns[i].m_rows, rowCount * sizeof(Data));
        for (int j = rowCount; j < newCount; j++) {
             rows[j].m_pData = NULL;
             rows[j].m_type = t->m_columns[i].m_type;
        }

        t->m_columns[i].m_rows = rows;

    }
    
    // Synchronisation des counts.
    for (int i = 0; i < t->m_colCount; i++)  
        t->m_columns[i].m_rowCount = t->m_rowCount = t->m_rowArraySize = newCount;
    
    return count;
}

/*
 * Deleter des éléments dans un array dynamic n'est pas très efficace.
 * Les
 */
int DeleteRow(Table * t, int i)
{
    assert(i < t->m_rowCount && "Out of range.");
    t->m_active[i] = FALSE;
    
    t->m_rowCount--;
    
    return 0;
}

int DeleteColumn(Table * t, int i)
{
    assert(i < t->m_colCount && "Out of range.");
    t->m_columns[i].m_active = FALSE;
    
    t->m_colCount--;
    
    return 0;
}

// Accessor
const Data * At(Table * t, int row, int column)
{
    assert(row < t->m_rowCount && column < t->m_colCount && "Out of range.");
    
    Data * d = & (t->m_columns[column].m_rows[row]);
    return d;
}

void generic_set(Table * t, int row, int column, void * dat, DataType expected) {
    assert(row < t->m_rowCount && column < t->m_colCount && "Out of range.");    
    assert(t->m_columns[column].m_rows[row].m_type == expected && "Invalid type.");
      
    
    t->m_columns[column].m_rows[row].m_pData = dat;
}

void SetInt(Table * t, int row, int column, int i){
    int * pI = malloc(sizeof(int));
    *pI = i;
    
    generic_set(t, row, column, pI, _INT);
}

void SetDouble(Table * t, int row, int column, double d) {
    double * dI = malloc(sizeof(double));
    *dI = d;
    
    generic_set(t, row, column, dI, _DOUBLE);
}
void SetString(Table * t, int row, int column, char * str){ 
    generic_set(t, row, column, str, _STRING);
}

int DeleteTable(Table * t)
{
    
}

const Data * GetRow(const Table const * t, int iRow)
{
    // On teste si la row existe vraiment
    iRow = IndexToRowIndex(t, iRow);
    if (iRow == invalid_index)
        return NULL;
    
    Data * curRow = (Data*)malloc( t->m_rowCount * sizeof(Data));
    
    int j = 0;
    for (int i = 0; i < t->m_colArraySize; i++) {
        if (!t->m_columns[i].m_active)
            continue;
        
        curRow[j++] = t->m_columns[i].m_rows[iRow];
    }
    
    return curRow;
}

const TableHeader * GetHeaders(const Table * t)
{
    TableHeader * hdrs = DYNARRAY(TableHeader, t->m_colCount);
    
    for (int i = 1; i <= t->m_colCount; i++)
    {
        int index = IndexToColIndex(t, i);
        if (index != invalid_index)
        {
            hdrs[i-1] = t->m_columns[index].m_header;
        }
        else
            return NULL;
    }
    
    return hdrs;
}
int IndexToRowIndex(const Table const * t, int iRow) 
{
    int dacc_active = iRow; // De-accumulator
    int acc_inactive = 0; // Accumulator
    
    int cursor = 0;
    
    while (dacc_active > 0)
    {
        if (cursor >= t->m_rowArraySize)
            return invalid_index;
        
        if (t->m_active[cursor] == TRUE)
            dacc_active--;
        else
            acc_inactive++;
        
        cursor++;
    }

    return (iRow + acc_inactive) - 1;
}

int IndexToColIndex(const Table const * t, int iCol)
{
    int dacc_active = iCol; // De-accumulator
    int acc_inactive = 0; // Accumulator
    
    int cursor = 0;    
    while (dacc_active > 0)
    {
        if (cursor >= t->m_colArraySize)
            return invalid_index;
        
        if (t->m_columns[cursor].m_active == TRUE)
            dacc_active--;
        else
            acc_inactive++;
        
        cursor++;
    }

    return (iCol + acc_inactive) - 1;
}