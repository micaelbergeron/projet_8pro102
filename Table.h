/* 
 * File:   Table.h
 * Author: Micael
 *
 * Created on 3 décembre 2011, 13:41
 */

#ifndef TABLE_H
#define	TABLE_H

#include <stdarg.h>
#include "datatype.h"

enum return_codes {
    invalid_index = -1
};

// Functions 
Table * CreateTable(int count, ...);
int AddColumns(Table * t, int count, ...);
int AddRows(Table * t, int count);

int DeleteRow(Table * t, int i);
int DeleteColumn(Table * t, int i);

const Data * At(Table * t, int row, int column);

// Generic Set
void generic_set(Table * t, int row, int column, void * dat, DataType expected);

void SetInt(Table * t, int row, int column, int d);
void SetDouble(Table * t, int row, int column, double d);
void SetString(Table * t, int row, int column, char * str);

int DeleteTable(Table * t);

// Other function
const TableHeader * GetHeaders(const Table const * t);
const Data * GetRow(const Table const * t, int iRow); 

int IndexToRowIndex(const Table const * t, int iRow);
int ColumnToColIndex(const Table const * t, int iCol);
#endif	/* TABLE_H */

