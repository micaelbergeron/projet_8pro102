/* 
 * File:   iomanip.h
 * Author: Micael
 *
 * Created on 4 décembre 2011, 21:36
 */

#ifndef IOMANIP_H
#define	IOMANIP_H

#include "Table.h"
#include "string.h"

#define BUFFER_SIZE 30
#define TAB Tab(1)

// Const chars for borders
static const char midSideBrd            = (char)186; // ║
static const char upLeftBrd             = (char)201; // ╔
static const char upRightBrd            = (char)187; // ╗
static const char botLeftBrd            = (char)200; // ╚
static const char botRightBrd           = (char)188; // ╝
static const char triLeftBrd            = (char)204; // ╠
static const char triRightBrd           = (char)185; // ╣
static const char triTopBrd             = (char)203; // ╦
static const char triBotBrd             = (char)202; // ╩
static const char midTopBrd             = (char)205; // ═

char * Tab(int n);

char * ToString(const Data const * d);
void PrintCell(const Data const * d);
void PrintColumn(const TableColumn const * tc);
void PrintTable(const Table const * t);
void PrintHeaders(const Table const * t);

#endif	/* IOMANIP_H */

